package FriendsAndFollowersAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import MongoUtilities.MongoAdapter;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;

import twitter4j.IDs;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.PagableResponseList;
public class GetFollowersIDsDirectWriter {
	
	public GetFollowersIDsDirectWriter() {
		super();
		// TODO Auto-generated constructor stub
	} 

    /**
     * Usage: java FriendsAndFollowersAdapter.GetFollowersIDs IDList<>
     *
     * @param args message
     * @return 
     */
 
    public synchronized void DirectWriterFollowersGet(List<Long> idList)
            {
    	
    	
			//declaring local mongodb adapter
			MongoAdapter myMongoAdapter = new MongoAdapter();
			DB mydb = myMongoAdapter.myMongoConnect("twitterdb04");
			
			
			//declaring  local mongodb collection and dbobj
	        DBCollection myIdMapCollection = mydb.getCollection("LevelAFollowers");
	        
	        BasicDBObject myObj = new BasicDBObject();
            
	        long _wait = 70000;    
            Map<Long, List<Long>> myMap = new HashMap<Long, List<Long>>();
            List<Long> followersList = new ArrayList<Long>();
            
            
            try {
                
                Twitter twitter = new TwitterFactory().getInstance();         
                long cursor = -1;
                IDs ids=null;     
                
                System.out.println("Listing followers's ids.");
               
                    do {
                            for(Long inid : idList){
                                
                                    ids = twitter.getFollowersIDs(inid, cursor);
                                    
                                   
                		            
                                    
                                    System.out.println("followers for id    " + inid);
                                try {
                                    
                                    wait(_wait);
                                } catch (InterruptedException ex) {
                                    Logger.getLogger(GetFollowersIDs.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                  int i = 0;
    	                            int j=0;
    	                            long[] l=null;
    	                    	while(j!=4000){
    	                    		
    	                    		l = ids.getIDs();
    	                    		
    	                            
    	                            j++;
    	                            System.out.println( l.toString() + "  count  " + i++);


    	                           }
                                myObj.clear();
    	                        myObj.put("user_id", inid);
    	                        myObj.put("followers_list", l);
    	                        myIdMapCollection.save(myObj);
    	                        long coll_count = myIdMapCollection.count();
    	                        System.out.println( coll_count + "  Collection Count  ");
                                    
  
                            
                            
                            }
                        } while ((cursor = ids.getNextCursor()) != 0);}
             
            
                catch (TwitterException te) {
                    te.printStackTrace();
                    boolean b = te.exceededRateLimitation();
                 
                    System.out.println("Rate Limit Exceeded is " + b);
                    System.out.println("Failed to get followers' ids: " + te.getMessage());
                    
                    
                }
            
	}

    public synchronized void DirectWriterFollowersGet(List<Long> idList, String DBname, String CollectionName)
    {


	//declaring local mongodb adapter
	MongoAdapter myMongoAdapter = new MongoAdapter();
	DB mydb = myMongoAdapter.myMongoConnect(DBname);
	
	
	//declaring  local mongodb collection and dbobj
    DBCollection myIdMapCollection = mydb.getCollection(CollectionName);
    
    BasicDBObject myObj = new BasicDBObject();
    
    long _wait = 60000;    
    Map<Long, long[]> myMap = new HashMap<Long, long[]>();
    List<Long> followersList = new ArrayList<Long>();
    
    
    try {
        
        Twitter twitter = new TwitterFactory().getInstance();         
        long cursor = -1;
        IDs ids=null;
        
        System.out.println("Listing followers's ids.");
        
       
            do {
                    for(Long inid : idList){
                        
                            ids = twitter.getFollowersIDs(inid, cursor);
                            
                            
                            System.out.println("followers for id    " + inid);
                        try {
                            
                            wait(_wait);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(GetFollowersIDs.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        int i = 0;
                          int j=0;
                          long[] l=null;
                  	while(j!=4000){
                  		
                  		l = ids.getIDs();
                  		
                          
                          j++;
                          System.out.println( l.toString() + "  count  " + i++);


                         }
                      myObj.clear();
                      myObj.put("user_id", inid);
                      myObj.put("followers_list", l);
                      myIdMapCollection.save(myObj);
                      long coll_count = myIdMapCollection.count();
                      System.out.println( coll_count + "  Collection Count  ");


                    
                    }
                } while ((cursor = ids.getNextCursor()) != 0);}
     
    
        catch (TwitterException te) {
            te.printStackTrace();
            boolean b = te.exceededRateLimitation();
         
            System.out.println("Rate Limit Exceeded is " + b);
            System.out.println("Failed to get followers' ids: " + te.getMessage());
            
            
        }
    
}


}
