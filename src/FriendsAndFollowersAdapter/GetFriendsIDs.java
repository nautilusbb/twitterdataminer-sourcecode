package FriendsAndFollowersAdapter;



import Twitter4j.Utilities.RateLimit;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import twitter4j.IDs;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Lists Friends' ids
 *
 * @author Osama Salama 
 */

public final class GetFriendsIDs {

    public GetFriendsIDs() {
        super();
    }

    /**
     * Usage: java FriendsAndFollowersAdapter.GetFriendsIDs IDList<>
     *
     * @param args message
     */
 
    public synchronized Map<Long, List<Long>> MapFriendsGet(List<Long> idList)
            {
                
            long _wait = 60000;    
            Map<Long, List<Long>> myMap = new HashMap<Long, List<Long>>();
            List<Long> friendsList = new ArrayList<Long>();
            
            
            try {
                
                Twitter twitter = new TwitterFactory().getInstance();         
                long cursor = -1;
                IDs ids=null;          
                System.out.println("Listing Friends ids.");
               
                    do {
                            for(Long inid : idList){
                                
                                    ids = twitter.getFriendsIDs(inid, cursor);
                                    
                                    
                                    
                                    System.out.println("friends for id    " + inid);
                                try {
                                    
                                    wait(_wait);
                                } catch (InterruptedException ex) {
                                    Logger.getLogger(GetFriendsIDs.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                    int i = 0;
                                    

                                for (long id : ids.getIDs()) {
                                    friendsList.add(id);
                                    System.out.println( id + "  count  " + i++);


                                    }
                                myMap.put(inid, friendsList);
                                    
  
                            
                            
                            }
                        } while ((cursor = ids.getNextCursor()) != 0);}
             
            
                catch (TwitterException te) {
                    te.printStackTrace();
                    boolean b = te.exceededRateLimitation();
                 
                    System.out.println("Rate Limit Exceeded is " + b);
                    System.out.println("Failed to get followers' ids: " + te.getMessage());
                    
                    
                }
            return myMap;
	}
}
