package FriendsAndFollowersAdapter;

import java.awt.print.Pageable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import MongoUtilities.MongoAdapter;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.Mongo;
import com.mongodb.WriteConcern;

import twitter4j.IDs;
import twitter4j.PagableResponseList;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.TwitterResponse;

public class GetFriendsIDsDirectWriter {
	public static String RelationLevel= "LevelAFriends";
	
	 public GetFriendsIDsDirectWriter() {
		// TODO Auto-generated constructor stub
		 
		        super();
		 
	} 

	    /**
	     * Usage: java FriendsAndFollowersAdapter.GetFriendsIDs IDList<>
	     *
	     * @param args message
	     */
	 //Gets friends ids and writes directly to mongodb in each get and wait iteration
	 public synchronized void DirectWriterFriendsGet(List<Long> idList)
	    {


		//declaring local mongodb adapter
		MongoAdapter myMongoAdapter = new MongoAdapter();
		DB mydb = myMongoAdapter.myMongoConnect("twitterdb03");
		
		
		//declaring  local mongodb collection and dbobj
	    DBCollection myIdMapCollection = mydb.getCollection("LevelAFriends");
	    
	    BasicDBObject myObj = new BasicDBObject();
	    
	    long _wait = 60000;    
	    Map<Long, long[]> myMap = new HashMap<Long, long[]>();
	    List<Long> friendsList = new ArrayList<Long>();
	    
	    
	    try {
	        
	        Twitter twitter = new TwitterFactory().getInstance();         
	        long cursor = -1;
	        IDs ids=null;
	        
	        System.out.println("Listing friends's ids.");
	        
	       
	            do {
	                    for(Long inid : idList){
	                        
	                            ids = twitter.getFriendsIDs(inid, cursor);
	                            
	                            
	                            System.out.println("friends for id    " + inid);
	                        try {
	                            
	                            wait(_wait);
	                        } catch (InterruptedException ex) {
	                            Logger.getLogger(GetFriendsIDs.class.getName()).log(Level.SEVERE, null, ex);
	                        }
	                            int i = 0;
	                            int j=0;
	                            long[] l=null;
	                            //loop to decrease BSON size to below 16MB limit to be able to insert BSON one by one
	                    	while(j!=4000){
	                    		
	                    		l = ids.getIDs();
	                    		
	                            
	                            j++;
	                            System.out.println( l.toString() + "  count  " + i++);


	                           }
	                    	myObj.clear();
	                        myObj.put("user_id", inid);
	                        myObj.put("friends_list", l);
	                        myIdMapCollection.save(myObj);
	                        long coll_count = myIdMapCollection.count();
	                        System.out.println( coll_count + "  Collection Count  ");


	                    
	                    }
	                } while ((cursor = ids.getNextCursor()) != 0);}
	     
	    
	        catch (TwitterException te) {
	            te.printStackTrace();
	            boolean b = te.exceededRateLimitation();
	         
	            System.out.println("Rate Limit Exceeded is " + b);
	            System.out.println("Failed to get friends' ids: " + te.getMessage());
	            
	            
	        }
	    
	}

	    public synchronized void DirectWriterFriendsGet(List<Long> idList, String DBname, String CollectionName)
	    {


		//declaring local mongodb adapter
		MongoAdapter myMongoAdapter = new MongoAdapter();
		DB mydb = myMongoAdapter.myMongoConnect(DBname);
		
		
		//declaring  local mongodb collection and dbobj
	    DBCollection myIdMapCollection = mydb.getCollection(CollectionName);
	    
	    BasicDBObject myObj = new BasicDBObject();
	    
	    long _wait = 120000;    
	    Map<Long, long[]> myMap = new HashMap<Long, long[]>();
	    List<Long> friendsList = new ArrayList<Long>();
	    
	    
	    try {
	        
	        Twitter twitter = new TwitterFactory().getInstance();         
	        long cursor = -1;
	        IDs ids=null;
	        
	        System.out.println("Listing friends's ids.");
	        
	       
	            do {
	                    for(Long inid : idList){
	                        
	                            ids = twitter.getFriendsIDs(inid, cursor);
	                            
	                            
	                            System.out.println("friends for id    " + inid);
	                        try {
	                            
	                            wait(_wait);
	                        } catch (InterruptedException ex) {
	                            Logger.getLogger(GetFriendsIDs.class.getName()).log(Level.SEVERE, null, ex);
	                        }
	                            int i = 0;
	                            int j=0;
	                            long[] l=null;
	                    	while(j!=4000){
	                    		
	                    		l = ids.getIDs();
	                    		
	                            
	                            j++;
	                            System.out.println( l.toString() + "  count  " + i++);


	                           }
	                    	myObj.clear();
	                        myObj.put("user_id", inid);
	                        myObj.put("friends_list", l);
	                        myIdMapCollection.save(myObj);
	                        long coll_count = myIdMapCollection.count();
	                        System.out.println( coll_count + "  Collection Count  ");


	                    
	                    }
	                } while ((cursor = ids.getNextCursor()) != 0);}
	     
	    
	        catch (TwitterException te) {
	            te.printStackTrace();
	            boolean b = te.exceededRateLimitation();
	         
	            System.out.println("Rate Limit Exceeded is " + b);
	            System.out.println("Failed to get friends' ids: " + te.getMessage());
	            
	            
	        }
	    
	}

}


