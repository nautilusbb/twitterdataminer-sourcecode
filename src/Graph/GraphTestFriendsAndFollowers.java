package Graph;

import java.awt.Dimension;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JFrame;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;

import FileImport.fileImporter;
import MongoUtilities.MongoGet;
import edu.uci.ics.jung.algorithms.layout.CircleLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.graph.util.EdgeType;
import edu.uci.ics.jung.*;
import edu.uci.ics.jung.graph.*;
import edu.uci.ics.jung.samples.SimpleGraphDraw;
import edu.uci.ics.jung.visualization.BasicVisualizationServer;


public class GraphTestFriendsAndFollowers extends DirectedSparseGraph<Long, Long> {
	

	public GraphTestFriendsAndFollowers() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public static void main(String[] args){
		
		//declaring te graph my graph
		DirectedSparseGraph<myNode, String> myGraph = new DirectedSparseGraph<myNode, String>();
		
		//ID list importing block
    	//fileImporter myImporter = new fileImporter();
    	//String s1="C:\\Users\\osama\\Documents\\Usable texts\\TrendingAccounts252 - Copy";
    	//List<Long> IdsList = myImporter.importIds(s1);
		
    	MongoGet myMongoGet = new MongoGet();
    	
    	
    	Map<Long, BasicDBList> FriendsIdsMap = new HashMap<Long, BasicDBList>();
    	Map<Long, BasicDBList> FollowersIdsMap = new HashMap<Long, BasicDBList>();
    	
		FriendsIdsMap=myMongoGet.GetIdsMap("twitterdb03", "LevelAFriends", "friends_list", 100);
    	FollowersIdsMap= myMongoGet.GetIdsMap("twitterdb03", "LevelAFollowers", "followers_list",100);
		
		
    	
    	for (Map.Entry<Long, BasicDBList> entry : FriendsIdsMap.entrySet()){
    		
    		Long L = entry.getKey();
            myNode Unode = new myNode(L);
    		if(!myGraph.containsVertex(Unode)){
    			myGraph.addVertex(Unode);	
    		}
    		BasicDBList RelatedList= (BasicDBList)entry.getValue();
    		for(Object myobj : RelatedList){

                Long F = Long.parseLong(myobj.toString());
    			myNode Fnode = new myNode(F);
    			String f = Unode.toString() + Fnode.toString();

    			if(!myGraph.containsVertex(Fnode)){
    				myGraph.addVertex(Fnode);
    				
    				if(!myGraph.containsEdge(f + Fnode.toString())){
    					
    				myGraph.addEdge(f, Unode, Fnode, EdgeType.DIRECTED);
    			}
    			
    		}	
    		

   		}
    		for (Map.Entry<Long, BasicDBList> entry1 : FriendsIdsMap.entrySet()){
        		
        		Long L1 = entry1.getKey();
        		myNode Unode1 = new myNode(L);
        		if(!myGraph.containsVertex(Unode1)){
        			myGraph.addVertex(Unode1);	
        		}
        		BasicDBList RelatedList1= (BasicDBList)entry1.getValue();
        		for(Object myobj1 : RelatedList1){
        			Long F1 = Long.parseLong(myobj1.toString());
        			myNode Fnode1 = new myNode(F1);
        			String f1 = Unode1.toString() + Fnode1.toString();
        			if(!myGraph.containsVertex(Fnode1)){
        				myGraph.addVertex(Fnode1);
        				
        				if(!myGraph.containsEdge(Fnode1.toString() + Unode1.toString())){
        				try{myGraph.addEdge(Fnode1.toString() + Unode1.toString(), Fnode1 ,Unode1, EdgeType.DIRECTED);}
        				catch (Exception ex){
        					ex.printStackTrace();
        				}
        			}
        			
        		}	
        		

       		}
    	int numberofVertecies = myGraph.getVertexCount();
    	int numberofEdges = myGraph.getEdgeCount();
    	List<Long> myNodes = new ArrayList<Long>();
    	/*

    	for (Map.Entry<Long, BasicDBList> entry2 : FriendsIdsMap.entrySet()){
    		Long L2 = entry2.getKey();
    		myNodes.add(L2);
    	}
    		
    	List<Integer> degrees = new ArrayList<Integer>();
    	for(Long l : myNodes ){
    		myNode node2 = new myNode(l);
    		degrees.add(myGraph.degree(node2));
    		
    		
    	}
    	
    		
    	System.out.println(degrees.toString());
        */
    	
    							
    	System.out.println(myGraph.toString() + " numberofEdges " + numberofEdges + " numberofVertecies " + numberofVertecies);


    		
   	}

    	}		
}
}


