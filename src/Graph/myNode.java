/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Graph;

import java.util.Collection;
import java.util.List;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MaximizeAction;

import AntColony.Density;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.util.Pair;

/**
 *
 * @author osama
 */
public class myNode extends edu.uci.ics.jung.graph.DirectedSparseGraph{
    public long _id;
    public BasicDBObject myObj;
    public Long userId;
    
    

    public myNode() {
        super();
    }
   
    public myNode(Long _id) {
        super();
        this._id = _id;
        
        
        
    }
    public double Imortance (myNode node, Graph myGraph){
    	
    	
    	int vdegreeOut = node.degree(node);
    	double maxG = myGraph.getEdgeCount(this.getDefaultEdgeType());
    	
    	double imp = ((double)vdegreeOut)/maxG;
    	
    	return imp;
    }
    
    public int NFDN (myNode node){
    	int nfdn=0;
    	Collection<myNode> neighbours =node.getNeighbors(node);
    	for(myNode nodeit : neighbours){
    		if(nodeit.getNeighborCount(nodeit)<=1){
    			nfdn++;
    		}
    	}
    	
    	
    	return 0;
    }        
       
    public int vertexCompatability(myNode node, Collection<myNode> Community){
		
    	Collection<myNode> incomingNodes = node.getInEdges(node);
    	int compvalue=0;
    	for(myNode nodeit : Community)
    	{
    	if(incomingNodes.contains(nodeit))
    	{
    		compvalue++;
    	}
    	}
    	
    	
    	return compvalue;
	}
    public int internalCommunityDegree (myNode node, Collection<myNode> Community){
    	int internalDegree=0;
    	
    	for(myNode nodeit : Community){
    		
    		//might need rivison
    		
    		if(nodeit.isNeighbor(node, nodeit)){
    			internalDegree++;
    			
    		}
    		
    	}
    	
    	return internalDegree;
    }
    
    public double CompatabilityS (myNode node, Collection<myNode> Community)
    {
    	int communityCompatability = this.vertexCompatability(node, Community);
    	double comp = Math.max(communityCompatability, this.NFDN(node));
    	return 0 ;
    }

	
	public double vertexDensity(myNode node, Collection<myNode> community) {
		double vDensity =0;
		
		vDensity = (double)node.internalCommunityDegree(node, community)/(double)node.CompatabilityS(node, community);
		
		return vDensity;
	}


    
}
