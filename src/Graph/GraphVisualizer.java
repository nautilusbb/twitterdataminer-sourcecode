package Graph;

import java.awt.Dimension;

import javax.swing.JFrame;

import edu.uci.ics.jung.algorithms.layout.CircleLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.samples.SimpleGraphDraw;
import edu.uci.ics.jung.visualization.BasicVisualizationServer;

public class GraphVisualizer {

	public void visualize()
	{
    	JFrame jf = new JFrame("My Graph View");
		
		 
		
		SimpleGraphDraw sgd = new SimpleGraphDraw();
		Layout<Long, Long> layout = new CircleLayout<Long,Long>(null);
		layout.setSize(new Dimension(600, 600));
		
		
        BasicVisualizationServer<Long, Long> vv = new BasicVisualizationServer<Long,Long>(layout); 
        
        vv.setPreferredSize(new Dimension(650, 650));
        
        jf.getContentPane().add(vv);
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jf.pack();
        jf.setVisible(true);

	}
	
}
