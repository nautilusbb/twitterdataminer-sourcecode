package Graph;

import java.awt.Dimension;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JFrame;

import Neo4j.Utilities.Neo4jEmbeddedServer;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;

import FileImport.fileImporter;
import MongoUtilities.MongoGet;
import edu.uci.ics.jung.algorithms.layout.CircleLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.graph.util.EdgeType;
import edu.uci.ics.jung.*;
import edu.uci.ics.jung.graph.*;
import edu.uci.ics.jung.samples.SimpleGraphDraw;
import edu.uci.ics.jung.visualization.BasicVisualizationServer;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.io.fs.FileUtils;
import  Tests.GetScreenNames;

public class GraphTestMain extends DirectedSparseGraph<Long, Long> {
	private static void registerShutdownHook( final GraphDatabaseService graphDb )
	{
		// Registers a shutdown hook for the Neo4j instance so that it
		// shuts down nicely when the VM exits (even if you "Ctrl-C" the
		// running application).
		Runtime.getRuntime().addShutdownHook( new Thread()
		{
			@Override
			public void run()
			{
				graphDb.shutdown();
			}
		} );
	}

	public GraphTestMain() {
		super();
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) throws IOException{


		DirectedSparseGraph<Long, Long> myGraph = new DirectedSparseGraph<Long, Long>();
		GraphDatabaseService graphDb = new GraphDatabaseFactory().newEmbeddedDatabase( "/Users/nautilus/Documents/Neo4j/default.graphdb" );
		registerShutdownHook( graphDb );
		Node neoNode;
		//ID list importing block
    	//fileImporter myImporter = new fileImporter();
    	//String s1="C:\\Users\\osama\\Documents\\Usable texts\\TrendingAccounts252 - Copy";
    	//List<Long> IdsList = myImporter.importIds(s1);
		
    	MongoGet myMongoGet = new MongoGet();
    	
    	Map<Long, BasicDBList> FriendsIdsMap =myMongoGet.GetIdsMap("twitterdb03", "LevelAFriends", "friends_list", 50);
    	//Map<Long, BasicDBList> FollowersIdsMap = new HashMap<Long, BasicDBList>();
    	GetScreenNames gsn = new GetScreenNames();

    	//FollowersIdsMap= myMongoGet.GetIdsMap("twitterdb03", "LevelAFollowers", "followers_list");
		try(Transaction tx = graphDb.beginTx()){
			for (Map.Entry<Long, BasicDBList> entry : FriendsIdsMap.entrySet()){
				Long L = entry.getKey();


					neoNode = graphDb.createNode();
					neoNode.setProperty("_id" , L);



				if(!myGraph.containsVertex(L)){
                    gsn.mygsn(L);

					myGraph.addVertex(L);
				}
				BasicDBList RelatedList= (BasicDBList)entry.getValue();
				for(Object myobj : RelatedList){
					if(!myGraph.containsVertex(Long.parseLong(myobj.toString()))){
						myGraph.addVertex(Long.parseLong(myobj.toString()));
//						neoNode.setProperty();
						if(!myGraph.containsEdge(L + Long.parseLong(myobj.toString())))
						myGraph.addEdge(L + Long.parseLong(myobj.toString()), L, Long.parseLong(myobj.toString()),EdgeType.DIRECTED);
						}

					}

				}
			tx.success();
   		}
    	int numberofVertecies = myGraph.getVertexCount();
    	int numberofEdges = myGraph.getEdgeCount();
    	/*
    	for(Long l : IdsList){
    		myNode node = new myNode(l);
    		int deg = node.degree(node);
    		System.out.println(l + deg);
    	}
    	*/
    	
        
    	
    							
    	System.out.println(myGraph.toString() + " numberofEdges " + numberofEdges + " numberofVertecies " + numberofVertecies);
    	
		
		 
		

    	

    		
   	}

		
}
	


