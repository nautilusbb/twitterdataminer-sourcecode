package FileImport;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.*;


public class fileImporter {

	 //private Scanner myScanner;
	 //private File myfile;
	/*public Scanner getMyScanner() {
		//return myScanner;
	}
	public void setMyScanner(Scanner myScanner) {
		this.myScanner = myScanner;
	}
	public File getMyfile() {
		return myfile;
	}
	public void setMyfile(File myfile) {
		this.myfile = myfile;
	}*/
	public fileImporter() {
		super();
		
	}
	public ArrayList<String> importHashtags(String fileName ){
		ArrayList<String> hashTracks = new ArrayList<String>();
		try {
			
			Scanner s = new Scanner(new File(fileName));
			   
			    while (s.hasNext()){
			      hashTracks.add(s.next());
			    }
			    s.close();
			    
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 if(hashTracks.isEmpty())
	 {return null;}
	else {return hashTracks;}
	 
	 

		}
	 
	public ArrayList<Long> importIds(String fileName ){
		ArrayList<Long> idTracks = new ArrayList<Long>();
		try {
			
			Scanner s = new Scanner(new File(fileName));
			   
			    while(s.hasNextLong())
			    {
			    	idTracks.add(s.nextLong());
			    }
			    s.close();
			    
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 if(idTracks.isEmpty())
	 {return null;}
	else {return idTracks;}
	 
	 

		}
}
