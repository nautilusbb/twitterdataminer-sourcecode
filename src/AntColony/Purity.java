package AntColony;


import java.util.*;


/**
 * Interface Purity
 */
public interface Purity {

  //
  // Fields
  //

  
  //
  // Methods
  //


  //
  // Accessor methods
  //

  //
  // Other methods
  //

  /**
   */
  public void vertexPurity(  );


  /**
   */
  public void communityPurity(  );


  /**
   */
  public void partitionPurity(  );


}
