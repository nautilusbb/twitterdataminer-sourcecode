package MongoUtilities;

import java.net.UnknownHostException;

import com.google.common.base.Splitter;
import com.google.common.collect.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Pattern;

import FileImport.fileImporter;
import Twitter4j.Utilities.RelatedListTrimmer;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

public class MongoGet {
	public MongoAdapter myAdapter;
	/**
	 * @param args
	 */
	public MongoGet() {
		super();
		
	}	
	
	
	//Getting collection names in a Mongo database
	public List<String> getdbCollections(String dbName){
		
		List<String> mCollectionNames = new ArrayList<String>();
		
		MongoAdapter myMongo = new MongoAdapter();
		DB mydb = myMongo.myMongoConnect(dbName);
		
		Set<String> colls = mydb.getCollectionNames();
		for (String s : colls) {
		 mCollectionNames.add(s);
		 System.out.println("DB "+ dbName + "Collections :" + s);
		}
		return mCollectionNames;
	}
	
	//Getting databases names in mongo instance
	public List<String> getdbNames() throws UnknownHostException{
		
		
		MongoClient mc = new MongoClient("localhost", 27017);
		
		List<String> dbNames  = new ArrayList<String>();
		
		
		for (String s : mc.getDatabaseNames()) {
			dbNames.add(s);
			System.out.println("Mongo Local Databases: "+ s);
		}
		return dbNames;
	}
	
	//Getting Id list from a Mongo Collection
	public List<Long> getIdList(String dbName, String collectionName){
		// Return list
		List<Long> idList = new ArrayList<Long>();
		
		
		
		//Initializing myMongo adapter 
		MongoAdapter myMongo = new MongoAdapter();
		DB mydb = myMongo.myMongoConnect(dbName);
		
		//Initializing my collection and DB object
		DBCollection mycoll = mydb.getCollection(collectionName);
		DBObject myObj = new BasicDBObject();
		
		
		//cursor to iterate over the collection
		
		DBCursor cursor = mycoll.find();
		try {
			   while(cursor.hasNext()) {
				   myObj= cursor.next();
				   if(myObj.containsField("user_id"))
						   {
                                             long l;          
                                             String s =  myObj.get("user_id").toString();
                                             l = Long.parseLong(s);
                                             idList.add(l);
                                                   
                                                   }
				  // System.out.println(cursor.next());
			      
			  }
			}
			catch(Exception ex){
				System.out.println("No more in this list " + ex.toString());
				
				}
				finally {
			
			   cursor.close();
			}	
		return idList;
        }
        

	//override reterning a list of arrays of long ids
	//Getting Id list from a Mongo Collection
    public List<Long> getIdListArray(String dbName, String collectionName){

        // Return list
        List<Long> idList = new ArrayList<Long>();
        List<Long> tempList = new ArrayList<Long>();

        //Initializing myMongo adapter
        MongoAdapter myMongo = new MongoAdapter();
        DB mydb = myMongo.myMongoConnect(dbName);

        //Initializing my collection and DB object
        DBCollection mycoll = mydb.getCollection(collectionName);
        DBObject myObj = new BasicDBObject();


        //cursor to iterate over the collection

        DBCursor cursor = mycoll.find();
        try {
               while(cursor.hasNext()) {
                   myObj= cursor.next();
                   if(myObj.containsField("user_id"))
                           {

                                             String s =  myObj.get("user_id").toString();
                                             long l = Long.parseLong(s);
                                             tempList.add(l);



                                                   }

                  // System.out.println(cursor.next());

              }
            }
            catch(Exception ex){
                System.out.println("No more in this list " + ex.toString());

                }
                finally {

               cursor.close();
            }
        return tempList;
        }

    public List<Long[]> MapgetIdListArray(String dbName, String collectionName, String RelationReturn){
        // Return list
        List<Long[]> idList = new ArrayList<Long[]>();
        List<Long> tempList = new ArrayList<Long>();

        //Initializing myMongo adapter
        MongoAdapter myMongo = new MongoAdapter();
        DB mydb = myMongo.myMongoConnect(dbName);

        //Initializing my collection and DB object
        DBCollection mycoll = mydb.getCollection(collectionName);
        DBObject myObj = new BasicDBObject();


        //cursor to iterate over the collection

        DBCursor cursor = mycoll.find();
        try {
               while(cursor.hasNext()) {
                   myObj= cursor.next();
                   if(myObj.containsField(RelationReturn))
                           {
                                myObj.get(RelationReturn);


                           }


                                    // System.out.println(cursor.next());

              }
            }
            catch(Exception ex){
                System.out.println("No more in this list " + ex.toString());

                }
                finally {

               cursor.close();
            }
        return null;
        }

    //Override Getting Id list limited to 15 documents
    public List<Long> getIdList(String dbName, String collectionName, int numberOfDocuments){
    // Return list
    List<Long> idList = new ArrayList<Long>();

    //Initializing myMongo adapter
    MongoAdapter myMongo = new MongoAdapter();
    DB mydb = myMongo.myMongoConnect(dbName);

    //Initializing my collection and DB object
    DBCollection mycoll = mydb.getCollection(collectionName);
    DBObject myObj = new BasicDBObject();


    //cursor to iterate over the collection

    DBCursor cursor = mycoll.find().limit(numberOfDocuments);
    try {
           while(cursor.hasNext()) {

               myObj= cursor.next();
               if(myObj.containsField("user_id"))
                       {
                                         long l;
                                         String s =  myObj.get("user_id").toString();
                                         l = Long.parseLong(s);
                                         idList.add(l);

                                               }
              // System.out.println(cursor.next());

          }
        }
        catch(Exception ex){
            System.out.println("No more in this list " + ex.toString());

            }
            finally {

           cursor.close();
        }
    return idList;}

    public Map<Long, Long> getRelatedIdsMap(String dbName, String collectionName, String RelationReturn){

        MongoAdapter myMongo = new MongoAdapter();
        DB mydb = myMongo.myMongoConnect(dbName);

        //Initializing my collection and DB object
        DBCollection mycoll = mydb.getCollection(collectionName);
        DBObject myObj = new BasicDBObject();
        long id = 0;

        MongoGet myMongoGet = new MongoGet();
        //return type default
        String s = RelationReturn;
        //types can either be
        //"friends_list"
        //or"followers_list"


        Map<Long,Long> RelationMap = new HashMap<Long,Long>();

        //Map<Long,Long[]> IdsMap = myMongoGet.GetIdsMap(dbName, collectionName, RelationReturn);


        //ID list importing block
        fileImporter myImporter = new fileImporter();
        String s1="C:\\Users\\osama\\Documents\\Usable texts\\Trendingaccounts252";


        List<Long> IdsList = myImporter.importIds(s1);


        //fetch collection
        DBCursor cursor = mycoll.find();
        //copy int a list of DBObjects
        List<DBObject> myDBObjectsList = cursor.toArray();
        //test existance of Ids in list of DB objects
        myDBObjectsList.containsAll(IdsList);

        for(Long UserId : IdsList ){
        for(DBObject ListObj : myDBObjectsList){

            ListObj.removeField("_id");
            Object CurrKey = ListObj.get("user_id");
            DBObject frnds =  (DBObject) ListObj.get(RelationReturn);
            String myArr = frnds.toString();


                if(myArr.contains(UserId.toString())){
                    RelationMap.put(UserId, (Long)CurrKey );

                }


            }

        }

        /*
        try {
               while(cursor.hasNext()) {
                   for(Long Id : IdsList){


                   myObj= cursor.next();
                   Object o = myObj.get(RelationReturn);
                   if (o.toString().contains(Id.toString())){
                       System.out.println("say something");
                       //do stuff here
                       Long userId = (Long) myObj.get("user_id");
                       RelationMap.put(Id, userId);

                   }


            }}}
            catch(Exception ex){
                System.out.println("No more in this list " + ex.toString());

                }
                finally {

               cursor.close();
            }
        */

        cursor.close();
        return RelationMap;
    }


    public Map<Long, BasicDBList> GetIdsMap (String dbName, String collectionName, String RelationReturn){

            //types can either be
            //"friends_list"
            //or"followers_list"

            Map<Long, BasicDBList> RelatedMap = new HashMap<Long, BasicDBList>();


            //Initializing myMongo adapter
            MongoAdapter myMongo = new MongoAdapter();
            DB mydb = myMongo.myMongoConnect(dbName);

            //Initializing my collection and DB object
            DBCollection mycoll = mydb.getCollection(collectionName);
            DBObject myObj = new BasicDBObject();

            BasicDBList myFriendsDBList = new BasicDBList();

            //cursor to iterate over the collection

            DBCursor cursor = mycoll.find();

            try {
                   while(cursor.hasNext()) {




                       myObj = cursor.next();
                       myObj.removeField("_id");

                        Long userId = (Long)myObj.get("user_id");

                        myFriendsDBList.addAll((BasicDBList)myObj.get(RelationReturn));

                        RelatedMap.put(userId,myFriendsDBList );



                }}
                catch(Exception ex){
                    System.out.println("No more in this list " + ex.toString());

                    }
                    finally {

                   cursor.close();
                }

            return RelatedMap;}
    public Map<Long, BasicDBList> GetIdsMap (String dbName, String collectionName, String RelationReturn, int numberOfIds){

        //types can either be
        //"friends_list"
        //or"followers_list"

        Map<Long, BasicDBList> RelatedMap = new HashMap<Long, BasicDBList>();


        //Initializing myMongo adapter
        MongoAdapter myMongo = new MongoAdapter();
        DB mydb = myMongo.myMongoConnect(dbName);

        //Initializing my collection and DB object
        DBCollection mycoll = mydb.getCollection(collectionName);
        DBObject myObj = new BasicDBObject();

        BasicDBList myFriendsDBList = new BasicDBList();

        //cursor to iterate over the collection

        DBCursor cursor = mycoll.find().limit(numberOfIds);

        try {
               while(cursor.hasNext()) {




                   myObj = cursor.next();
                   myObj.removeField("_id");

                    Long userId = (Long)myObj.get("user_id");

                    myFriendsDBList.addAll((BasicDBList)myObj.get(RelationReturn));



                    RelatedMap.put(userId,myFriendsDBList);



            }}
            catch(Exception ex){
                System.out.println("No more in this list " + ex.toString());

                }
                finally {

               cursor.close();
            }

        return RelatedMap;}

    public Map<Long, BasicDBList> GetIdsMap (String dbName, String collectionName, String RelationReturn, int numberOfIds, int maximumNumberofRelated){

        //types can either be
        //"friends_list"
        //or"followers_list"

        Map<Long, BasicDBList> RelatedMap = new HashMap<Long, BasicDBList>();


        //Initializing myMongo adapter
        MongoAdapter myMongo = new MongoAdapter();
        DB mydb = myMongo.myMongoConnect(dbName);

        //Initializing my collection and DB object
        DBCollection mycoll = mydb.getCollection(collectionName);
        DBObject myObj = new BasicDBObject();

        BasicDBList myFriendsDBList = new BasicDBList();

        //cursor to iterate over the collection

        DBCursor cursor = mycoll.find().limit(numberOfIds);

        try {
               while(cursor.hasNext()) {




                   myObj = cursor.next();
                   myObj.removeField("_id");

                    Long userId = (Long)myObj.get("user_id");

                    myFriendsDBList.addAll((BasicDBList)myObj.get(RelationReturn));


                    RelatedListTrimmer myTrimmer = new RelatedListTrimmer();
                    BasicDBList trimmedList = myTrimmer.TrimList(myFriendsDBList, maximumNumberofRelated);

                    RelatedMap.put(userId,trimmedList);



            }}
            catch(Exception ex){
                System.out.println("No more in this list " + ex.toString());

                }
                finally {

               cursor.close();
            }

        return RelatedMap;}




    public Map<Long, List<String>> GetFriendsIdsMap (String dbName, String collectionName, String RelationReturn){



        StringtoArray myStringtoArray = new StringtoArray();


        //types can either be
        //"friends_list"
        //or"followers_list"


        Map<Long, List<String>> FollowersMap = new HashMap<Long, List<String>>();
        List<String> IdsList = new ArrayList<String>();

            //Initializing myMongo adapter
            MongoAdapter myMongo = new MongoAdapter();
            DB mydb = myMongo.myMongoConnect(dbName);

            //Initializing my collection and DB object
            DBCollection mycoll = mydb.getCollection(collectionName);
            DBObject myObj = new BasicDBObject();

                //cursor to iterate over the collection

            DBCursor cursor = mycoll.find();
            try {
                   while(cursor.hasNext()) {




                       myObj = cursor.next();
                       myObj.removeField("_id");

                        Long userId = (Long)myObj.get("user_id");






                       FollowersMap.put(userId,IdsList );



                }}
                catch(Exception ex){
                    System.out.println("No more in this list " + ex.toString());

                    }
                    finally {

                   cursor.close();
                }

            return FollowersMap;}

        
        }
        


