package MongoUtilities;
import java.util.ArrayList;
import java.util.List;

import twitter4j.*;

import com.mongodb.DB;
import com.mongodb.Mongo;
import MongoUtilities.*;


public class IdentityChecker {
	public static void main(String[] args) {
       
		
		MongoGet myGetAdapter = new MongoGet();
		
		List<Long> myidsList = myGetAdapter.getIdListArray("twitterdb02","FilteredTweets02");
		List<Long> unprotectedIdsList = new ArrayList<>();
		List<Long> protectedIdsList = new ArrayList<>();
		
		
		int i=0;
		for (Long id : myidsList){
			
        try {
            Twitter twitter = new TwitterFactory().getInstance();
            long[] IdArray = null;
            IdArray[i]=id;
            i++;
            ResponseList<User> users = twitter.lookupUsers(IdArray);
            for (User user : users) {
                if (user.isProtected() != false) {
                    System.out.println("@" + user.getScreenName() + i +" - " + "is not protected");
                    unprotectedIdsList.add(user.getId());
                
                } else {
                    // the user is protected
                	protectedIdsList.add(user.getId());
                    System.out.println("this is user is protected@" + user.getScreenName());
                }
            }
            
            //writing to mongo list of unprotected ids
           
            MongoFWriter myIdsWriter = new MongoFWriter();
            myIdsWriter.myMongoIdWriter(unprotectedIdsList, "UnprotectedIds");
            
            
            System.out.println("Successfully looked up users [" + args[0] + "].");
            System.exit(0);
        } catch (TwitterException te) {
            te.printStackTrace();
            System.out.println("Failed to lookup users: " + te.getMessage());
            System.exit(-1);
        	}
		}
    }

	
}

