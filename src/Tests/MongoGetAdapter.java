package Tests;

import java.io.IOException;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.WriteConcern;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.DBCursor;
import com.mongodb.ServerAddress;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bson.BSONObject;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import java.util.*;
import MongoUtilities.MongoAdapter;


public class MongoGetAdapter {
		/*
		 * try{
				
				//specifiying mongo connection
				
				mongoClient = new MongoClient("localhost", 27017);
				db = mongoClient.getDB( "mytwitterdb01" );
				
		}
		 catch(IOException ex){
			ex.printStackTrace();
		}
		 */
	public static void main(String[] args){
		List<String> idList = new ArrayList<String>();
		
		
		//Mongo connectino using MongoAdapter
		MongoAdapter myMongo = new MongoAdapter();
		DB mydb = myMongo.myMongoConnect("twitterdb02");
							
			
		//cursor to get all documents in a collection
		DBCollection coll = mydb.getCollection("FilteredTweets01");
		DBObject dbObj = new BasicDBObject();
		DBCursor cursor = coll.find();
		try {
		   
			while(cursor.hasNext()) {
			   dbObj= cursor.next();
			   if(dbObj.containsField("user_id"))
					   {
				   idList.add(dbObj.get("user_id").toString());}
		   			System.out.println(cursor.next());
		      
		  }
			
		} catch (Exception ex){
			
			System.out.println("No more in this list " + ex.toString());
		  }finally {
		   cursor.close();
		}
		}
					
}




