package Tests;

/*
 * Copyright 2007 Yusuke Yamamoto
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import MongoUtilities.MongoAdapter;
import MongoUtilities.TwitEntity;
import twitter4j.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;


import org.bson.BSONObject;
import org.omg.CORBA.OBJECT_NOT_EXIST;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.BasicDBList;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;

import FileImport.fileImporter;


public final class PrintFilterStreamFriends extends fileImporter {
    public static void main(String[] args) throws TwitterException {
        
        
            
		MongoAdapter myMongo = new MongoAdapter();
		DB mydb = myMongo.myMongoConnect("twitterdb05");
    	
                final DBCollection coll = mydb.getCollection("FilteredTweetsFriends0325jan");
                
        StatusListener listener = new StatusListener() {
            int i = 0;
            @Override
            public void onStatus(Status status) {
                
                i++;
                System.out.println("@" + status.getUser().getScreenName() + " - " + status.getText() + i);
                
                BasicDBObject dbObj = new BasicDBObject();
                dbObj.put("id_str", status.getId());
                dbObj.put("name", status.getUser().getName());
                dbObj.put("user_id", status.getUser().getId());
                dbObj.put("text", status.getText());
                dbObj.put("conts", status.getContributors());
                dbObj.put("created_at", status.getCreatedAt());
                dbObj.put("current_user_retweet_id", status.getCurrentUserRetweetId());
                
                dbObj.put("lang", status.getIsoLanguageCode());
                
                
                //Converting twitter entities into lists
                URLEntity[] urls =  status.getURLEntities();
                HashtagEntity[] hashtags = status.getHashtagEntities();
                UserMentionEntity [] mentions = status.getUserMentionEntities();
                
                
                //creating entity lists
                ArrayList<String> myhashtags = new ArrayList<String>();
                ArrayList<String> myurls = new ArrayList<String>();
                ArrayList<String> mymentions = new ArrayList<String>();
                TwitEntity myEntity = new TwitEntity();
                
                //calling EntitiesAsList method from TwitEntity class
                myhashtags = myEntity.EntitiesAsList(hashtags);
                myurls=myEntity.EntitiesAsList(urls);
                mymentions=myEntity.EntitiesAsList(mentions);
                
                dbObj.put("hashtag_entities", myhashtags);
                dbObj.put("urls_entities", myurls);
                dbObj.put("mention_entities", mymentions);
                dbObj.put("retweet_count", status.getRetweetCount());
                dbObj.put("in_reply_to_user_id", status.getInReplyToUserId());
                dbObj.put("in_reply_to_status_id", status.getInReplyToStatusId());
                dbObj.put("source", status.getSource());
                  coll.insert(dbObj);
                
          
            }

            @Override
            public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
                System.out.println("Got a status deletion notice id:" + statusDeletionNotice.getStatusId());
            }

            @Override
            public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
                System.out.println("Got track limitation notice:" + numberOfLimitedStatuses);
            }

            @Override
            public void onScrubGeo(long userId, long upToStatusId) {
                System.out.println("Got scrub_geo event userId:" + userId + " upToStatusId:" + upToStatusId);
            }

            @Override
            public void onStallWarning(StallWarning warning) {
                System.out.println("Got stall warning:" + warning);
            }

            @Override
            public void onException(Exception ex) {
                ex.printStackTrace();
            }
        };

        TwitterStream twitterStream = new TwitterStreamFactory().getInstance();
        twitterStream.addListener(listener);
        
        //importing hashtag trackers
        ArrayList<Long> IdsList = new ArrayList<Long>();
        ArrayList<String> hashtagList = new ArrayList<String>();
    	
        //local file import utility
        fileImporter twitImport = new fileImporter();
    	
        
        //importing trending account and hashtags
        String s1="C:\\Users\\osama\\Documents\\Usable texts\\TrendingAccounts252";
    	IdsList= twitImport.importIds(s1);
    	
        //hashtagList = twitImport.importHashtags("C:/Users/osama/Documents/Usable texts/Trendinghashtags#rab3a");  
     
        //copying the checked follow array into the post filter status method param
	        long[] followArray = new long[IdsList.size()];
	        for (int i = 0; i < IdsList.size(); i++) {
	            followArray[i] = IdsList.get(i);
	        }
        
        //copying the checked track array into the post filter status method param
        String[] trackArray = hashtagList.toArray(new String[hashtagList.size()]);

        // filter() method internally creates a thread which manipulates TwitterStream and calls these adequate listener methods continuously.
        twitterStream.filter(new FilterQuery(0, followArray));
    	} 

    private static boolean isNumericalArgument(String argument) {
        String args[] = argument.split(",");
        boolean isNumericalArgument = true;
        for (String arg : args) {
            try {
                Integer.parseInt(arg);
            } catch (NumberFormatException nfe) {
                isNumericalArgument = false;
                break;
            }
        }
        return isNumericalArgument;
    }
}