/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Tests;

import FriendsAndFollowersAdapter.GetFollowersIDs;
import MongoUtilities.MongoAdapter;
import MongoUtilities.MongoFWriter;
import MongoUtilities.MongoGet;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *
 * @author homepc
 */
public class FollowersMapTest {
    public static void main(String[] args){
        //get followers utilitiy
        GetFollowersIDs  myFollowersGet = new GetFollowersIDs();        
        
        //Mongo Utilities Objects
        MongoAdapter myMongoAdapter = new MongoAdapter();
        MongoGet myGetAdapter = new MongoGet();
        
        //Mongodb connection
        DB mydb = myMongoAdapter.myMongoConnect("twitterdb02");
        
        //MongoDB 
        DBCollection myIdMapCollection = mydb.getCollection("LevelAFollowers");
        BasicDBObject myObj = new BasicDBObject();
               
        
        
        Map<Long, List<Long>> myMap = new HashMap<Long, List<Long>>();
        List<Long> idList = new ArrayList<Long>();
        
        
        
        
        idList=myGetAdapter.getIdList("twitterdb02", "FilteredTweets02");
        
        
        myMap=myFollowersGet.MapFollowersGet(idList);
        
        MongoFWriter myFwriter = new MongoFWriter();
        
        myFwriter.myMongoFWriter(myMap, "LevelAFollowers");
        
    }
}
