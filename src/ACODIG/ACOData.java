package ACODIG;

import java.util.Collection;


import java.util.List;

import Graph.myNode;
import edu.uci.ics.jung.graph.Graph;

public class ACOData {

	public ACOData() {
		super();
		// TODO Auto-generated constructor stub
	}

	public class myVertex extends edu.uci.ics.jung.graph.DirectedSparseGraph{
		public int _id;
		public myVertex(int _id){
			super();
			this._id=_id;
		}
		@Override
		
		public String toString() {

			return "myVertex [_id=" + _id + "]";
		}
		
		public int Vdegree (myVertex v, Graph myGraph){

			int vdegree = myGraph.degree(v);
			return 0;
		}

		public double Imortance (myVertex v, Graph myGraph){
	    	
	    	
	    	int vdegreeOut = myGraph.getOutEdges(v).size();
	    	double maxG = (double)myGraph.getEdgeCount();
	    	
	    	double imp = ((double)vdegreeOut)/maxG;
	    	
	    	return imp;
	    }
	    
      
	       
	    public int vertexCompatabilityC(myVertex v, myVertex[] community){
	    	int compvalue=0;
	    	
	    	for(int i = 0 ; i<community.length;i++){
	    	myVertex temp = community[i];
	    		compvalue += v.getInEdges(temp).size();
	    	}
	    	

	    	return compvalue;
		}
	    
	    public int internalCommunityDegree (myVertex v, myVertex[] community){
	    	int internalDegree=0;
	    	
	     	for(int i = 0 ; i<community.length;i++){
		    	myVertex temp = community[i];
		    	internalDegree += v.getInEdges(temp).size();
		    	internalDegree += v.getOutEdges(temp).size();	
		    	}
	    	
	    	return internalDegree;
	    }
	    
	    public double CompatabilityS (myVertex v, List<myVertex[]> partition, myVertex[] nfdn)
	    {
	    	int communityCompatability=0;
	    	for(myVertex[] community : partition){
	    		int i = 0;
	    		i = vertexCompatabilityC(v, community);
	    		 communityCompatability+=i;
	    	}
	    	
	    	double comp = Math.max(communityCompatability, nfdn.length);
	    	return comp;
	    	
    	}

		
		public double vertexDensity(myVertex v, myVertex[] community,Graph myGraph) {
			
			double vDensity =0;
			
			vDensity= vertexCompatabilityC(v, community)/Vdegree(v, myGraph);
			
			return vDensity;
		}
		
		public boolean vertexPurity(myVertex v, myVertex[] community, myVertex[] nfdn, List<myVertex[]> partition){
			
			double compSC = 0;
			double compS = 0;
			compSC = vertexCompatabilityC(v, community);
			compS = CompatabilityS(v, partition, nfdn);
			if(compSC==compS){
				return true;
			}else
				return false;
			
		}
		
	}
	public class myLink{
		public int _id;
		public myLink(int _id){
			this._id=_id;
		}
		@Override
		
		public String toString() {
			return "myLink [_id=" + _id + "]";
		}
		
	}
	
	
}
